const ACTIVE_CLASS = "active";

//  add active class in selected list item
const list = document.querySelectorAll(".navigation li");

function activeLink(event) {
  list.forEach((item) => item.classList.remove(ACTIVE_CLASS));

  //   this.classList.add(ACTIVE_CLASS);
  event.target.closest("li")?.classList.add(ACTIVE_CLASS); // move agnostique
}

list.forEach((item) => item.addEventListener("mousedown", activeLink));

// Menu toggle
const toggle = document.querySelector(".toggle");
const navigation = document.querySelector(".navigation");
const main = document.querySelector(".main");

toggle.addEventListener("click", () => {
  navigation.classList.toggle(ACTIVE_CLASS);
  main.classList.toggle(ACTIVE_CLASS);
});
